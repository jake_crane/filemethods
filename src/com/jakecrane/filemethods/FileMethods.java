package com.jakecrane.filemethods;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class FileMethods {

	public static final SimpleDateFormat SDF = new SimpleDateFormat("M/d/y h:mm aa");

	public static String md5sumofDirsFiles(ArrayList<FileAndSum> fileAndSums) throws IOException {
		try (StringWriter writer = new StringWriter()) {
			for (FileAndSum fileAndSum : fileAndSums) {
				writer.write(fileAndSum.getMd5sum() + System.lineSeparator());
			}
			/*try (BufferedWriter fileWriter = new BufferedWriter(new FileWriter("dirsums2.txt", true))) {//TODO remove
				fileWriter.write(writer.toString());
				System.out.println("File:\n" + new String(Files.readAllBytes(Paths.get("dirsums2.txt"))) + "end file");
				return md5sum(new File("dirsums2.txt"));
			}*/
			return md5sum(writer.toString());
		}
	}

	public static String md5sumRepresentingDir2(File topmostDir) throws NoSuchAlgorithmException, IOException {
		ArrayList<FileAndSum> children = new ArrayList<FileAndSum>();
			collectChildrenInfo(topmostDir, children);
			return md5sumofDirsFiles(children);
	}

	/**
	 * A temporary file is created while this method is being called.
	 * Unless an error occurs, the file will be deleted upon the method's completion.
	 * Does not guarantee exact file names or file structure.
	 * @param topmostDir
	 * @param writer
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	public static void writeFilesAndSums(File topmostDir, Writer writer) throws IOException, NoSuchAlgorithmException {
		ArrayList<FileAndSum> children = new ArrayList<FileAndSum>();
		writeFilesAndSums(topmostDir, writer, children);
		writer.write("\"" + topmostDir.getAbsolutePath() + "\" "
				+ md5sumofDirsFiles(children)
				+ " "
				+ FileMethods.SDF.format(new Long(topmostDir.lastModified()))
				+ System.lineSeparator());

	}

	public static void collectChildrenInfo(File topmostDir, ArrayList<FileAndSum> fileInfos) throws IOException, NoSuchAlgorithmException {
		try {
			for (File file : topmostDir.listFiles()) {
				if (file.isDirectory()) {
					ArrayList<FileAndSum> children = new ArrayList<FileAndSum>();
					collectChildrenInfo(file, children);
					FileAndSum fas = new FileAndSum(file, md5sumofDirsFiles(children));
					fileInfos.add(fas);
				} else {
					FileAndSum fas = new FileAndSum(file, FileMethods.md5sum(file));
					fileInfos.add(fas);
				}
			}	} catch (NullPointerException e) {
			System.err.println("Unable to list files for " + topmostDir.getAbsolutePath());
			//e.printStackTrace();
		}
	}

	public static void writeFilesAndSums(File topmostDir, Writer writer, ArrayList<FileAndSum> fileInfos) throws IOException, NoSuchAlgorithmException {
		try {
			for (File file : topmostDir.listFiles()) {
				if (file.isDirectory()) {
					ArrayList<FileAndSum> children = new ArrayList<FileAndSum>();
					writeFilesAndSums(file, writer, children);
					writer.write("\"" + file.getAbsolutePath() + "\" "
							+ md5sumofDirsFiles(children)
							+ " "
							+ FileMethods.SDF.format(new Long(file.lastModified()))
							+ System.lineSeparator());
				} else {
					FileAndSum fas = new FileAndSum(file, FileMethods.md5sum(file));
					fileInfos.add(fas);
					writer.write("\"" + fas.getFile().getAbsolutePath() + "\" "
							+ fas.getMd5sum()
							+ " "
							+ FileMethods.SDF.format(new Long(fas.getFile().lastModified()))
							+ System.lineSeparator());
				}
			}
		} catch (NullPointerException e) {
			System.err.println("Unable to list files for " + topmostDir.getAbsolutePath());
			//e.printStackTrace();
		}
	}

	/**
	 *
	 * @param topmostDir
	 * @param writer
	 * @throws IOException
	 */
	/*public static void writeFilesAndSums(File topmostDir, Writer writer) throws IOException {
		try {
		for (File file : topmostDir.listFiles()) {
			if (file.isDirectory()) {
				writeFilesAndSums(file, writer);
			} else {
				writer.write("\"" + file.getAbsolutePath() + "\" "
						+ FileMethods.md5sum(file)
						+ " "
						+ FileMethods.SDF.format(new Long(file.lastModified()))
						+ System.lineSeparator());
			}
		}
		} catch (NullPointerException e) {
			System.err.println("Unable to list files for " + topmostDir.getAbsolutePath());
			//e.printStackTrace();
		}
	}*/

	/**
	 *
	 * @param file
	 * @param pattern
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static boolean lineInFileContains(File file, Pattern pattern) throws FileNotFoundException, IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = null;
			while ((line = br.readLine()) != null) {
				if (pattern.matcher(line).find()) {
					return true;
				}
			}
		} catch (IOException e) {
			System.err.println("unable to read " + file.getAbsolutePath() + ": " + e.getLocalizedMessage());
		}
		return false;
	}

	/**
	 *
	 * @param file
	 * @param string
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static boolean lineInFileContains(File file, String string) throws FileNotFoundException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = null;
			while ((line = br.readLine()) != null) {
				if (line.contains(string)) {
					return true;
				}
			}
		} catch (IOException e) {
			System.err.println("unable to read " + file.getAbsolutePath() + ": " + e.getLocalizedMessage());
		}
		return false;
	}

	/**
	 * Unlike the string version of replaceAll, this method can modify line separators
	 * Pass '\0' for c2 to remove characters
	 * @param f
	 * @param c1
	 * @param c2
	 * @return
	 */
	public static boolean replaceAll(File f, char c1, char c2) {
		File tempFile = new File("replaceAlltemp");
		if (tempFile.exists() && !tempFile.delete()) {
			System.err.println("unable to delete " + tempFile.getAbsolutePath());
			return false;
		}
		try (BufferedReader reader = new BufferedReader(new FileReader(f))) {
			try (BufferedWriter output = new BufferedWriter(new FileWriter(tempFile))) {
				int c = -1;
				while ((c = reader.read()) != -1) {
					if (c == c1) {
						c = c2;
					}
					if (c != '\0') { //allow removal of character by passing in null char
						output.write(c);
					}
				}
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (!f.delete()) {
			System.err.println("unable to delete " + f.getAbsolutePath());
			return false;
		}
		if (!tempFile.renameTo(f)) {
			System.err.println("unable to rename " + tempFile.getAbsolutePath() + " to " + f.getAbsolutePath());
			return false;
		}
		return true;
	}

	/**
	 * Can't replace across multiple lines
	 * Can't modify line separators
	 * new file will end with new line
	 * @param f
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static boolean replaceAll(File f, String s1, String s2) {
		File tempFile = new File("replaceAlltemp");
		if (tempFile.exists() && !tempFile.delete()) {
			System.err.println("unable to delete " + tempFile.getAbsolutePath());
			return false;
		}
		try (BufferedReader reader = new BufferedReader(new FileReader(f))) {
			try (BufferedWriter output = new BufferedWriter(new FileWriter(tempFile))) {
				String line = null;
				while ((line = reader.readLine()) != null) {
					output.write(line.replaceAll(s1, s2) + System.lineSeparator());
				}
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (!f.delete()) {
			System.err.println("unable to delete " + f.getAbsolutePath());
			return false;
		}
		if (!tempFile.renameTo(f)) {
			System.err.println("unable to rename " + tempFile.getAbsolutePath() + " to " + f.getAbsolutePath());
			return false;
		}
		return true;
	}

	/**
	 * always appends to outputFile
	 * @param dirToSearch
	 * @param outputFile
	 * @throws IOException
	 */
	public static void writeFileTreeToFile(File dirToSearch, File outputFile, boolean includeTimeStamps) throws IOException {
		try (BufferedWriter br = new BufferedWriter(new FileWriter(outputFile, true))) {
			writeFileTree(dirToSearch, br, includeTimeStamps);
		}
	}

	/**
	 * Wrapper method for writeFileTree(File dirToSearch, Writer writer, String tabs)
	 * example usage:
	 * try (BufferedWriter br = new BufferedWriter(new OutputStreamWriter(System.out))) {
	 *		FileMethods.writeFileTree(dir1, br, true);
	 *	}
	 *	try (BufferedWriter br = new BufferedWriter(new FileWriter(new File("filename.txt"), true))) {
	 *		FileMethods.writeFileTree(dir1, br, true);
	 *	}
	 * @param dirToSearch
	 * @param outputFile
	 * @throws IOException
	 */
	public static void writeFileTree(File dirToSearch, Writer writer, boolean includeTimeStamps) throws IOException {
		writeFileTree(dirToSearch, writer, "", includeTimeStamps);
	}

	/**
	 *
	 * @param dirToSearch
	 * @param outputFile
	 * @param tabbs
	 * @throws IOException
	 */
	private static void writeFileTree(File dirToSearch, Writer writer, String tabs, boolean includeTimeStamps) throws IOException {
		writer.write(tabs + dirToSearch.getName() + System.lineSeparator());
		if (dirToSearch.listFiles() != null) {
			for (File currentFile : dirToSearch.listFiles()) {
				if (currentFile.isDirectory()) {
					writeFileTree(currentFile, writer, tabs + "\t", includeTimeStamps);
				} else {
					if (includeTimeStamps) {
						BasicFileAttributes attributes = Files.readAttributes(currentFile.toPath(), BasicFileAttributes.class);
						writer.write("\t" + tabs + currentFile.getName() + " " + md5sum(currentFile)
								+ " Created: " + SDF.format(new Long(attributes.creationTime().toMillis()))
								+ " Modified: " + SDF.format(new Long(currentFile.lastModified()))
								+ " Accessed: " + SDF.format(new Long(attributes.lastAccessTime().toMillis()))
								+ System.lineSeparator());
					} else {
						writer.write("\t" + tabs + currentFile.getName() + " " + md5sum(currentFile) + System.lineSeparator());
					}
				}
			}
		}
	}

	/**
	 * Writes the md5 sum of all files contained within the directory to a file.
	 * This includes subdirectories and their contents.
	 * @param dir
	 * @param outputFile
	 * @throws IOException
	 */
	public static void writeMd5sumsOfAllFilesToFile(File dir, File outputFile) throws IOException {
		try (BufferedWriter br = new BufferedWriter(new FileWriter(outputFile, true))) {
			writeMd5sumsOfAllFiles(dir, br);
		}
	}

	/**
	 *
	 * @param dir
	 * @param writer
	 * @throws IOException
	 */
	public static void writeMd5sumsOfAllFiles(File dir, Writer writer) throws IOException {
		if (dir.listFiles() != null) {
			for (File currentFile : dir.listFiles()) {
				if (!currentFile.isDirectory()) {
					writer.write(md5sum(currentFile) + System.lineSeparator());
				} else {
					writeMd5sumsOfAllFiles(currentFile, writer);
				}
			}
		}
	}

	/**
	 * A temporary file is created while this method is being called.
	 * Unless an error occurs, the file will be deleted upon the method's completion.
	 * Does not guarantee exact file names or file structure.
	 * @param dir
	 * @return
	 * @throws IOException
	 */
	public static String md5sumRepresentingDir(File dir) throws IOException {
		File temp = new File("dirSums" + ".txt");

		boolean fileCreated = false;
		do {//This loop with sleep is a bad fix for a sometimes occurring IOExcpetion
			try {
				if (!temp.createNewFile()) {
					System.err.println("unable to create " + temp.getAbsolutePath());
					System.exit(1);
				}
				fileCreated = true;
			} catch (IOException e) {
				System.out.println("Waiting on OS to create " + temp + "...");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		} while(!fileCreated);

		writeMd5sumsOfAllFilesToFile(dir, temp);
		String result = md5sum(temp);
		if (temp.exists() && !temp.delete()) {
			System.err.println("unable to delete " + temp.getAbsolutePath());
			System.exit(1);
		}
		return result;
	}

	/**
	 * Two temporary files are created while this method is being used.
	 * Unless an error occurs, the files will be deleted upon the method's completion.
	 * @param dir1
	 * @param dir2
	 * @return Returns true if the files in dir 1 and dir2 are the same.
	 * @throws IOException
	 */
	public static boolean dirContentsAreEqual(File dir1, File dir2) throws IOException {
		File dir1Sums = new File("dir1sums.txt");
		dir1Sums.createNewFile();
		File dir2Sums = new File("dir2sums.txt");
		dir2Sums.createNewFile();

		writeMd5sumsOfAllFilesToFile(dir1, dir1Sums);
		writeMd5sumsOfAllFilesToFile(dir2, dir2Sums);

		boolean result = md5sum(dir1Sums).equals(md5sum(dir2Sums));

		dir1Sums.delete();
		dir2Sums.delete();

		return result;
	}

	/**
	 * Searches recursively from the root directory and adds matching results to the outputList.
	 * @param dirToSearch
	 * @param searchTerm
	 * @param outputList
	 * @throws IOException
	 */
	public static void find(File dirToSearch, FileFilter filter, List<File> outputList) {
		try {
			for (File currentFile : dirToSearch.listFiles()) {
				if (filter.accept(currentFile)) {
					outputList.add(currentFile);
				}
				if (currentFile.isDirectory()) {
					find(currentFile, filter, outputList);
				}
			}
		} catch (NullPointerException e) {
			System.err.println("unable to list files for " + dirToSearch);
		}
	}

	/**
	 * Searches recursively from the root directory and writes matches to stdout.
	 * @throws IOException
	 */
	public static void find(final File dirToSearch, final FileFilter filter) throws IOException {
		try (BufferedWriter br = new BufferedWriter(new OutputStreamWriter(System.out))) {
			find(dirToSearch, filter, br);
		}
	}

	/**
	 * Searches recursively from the root directory and writes matches.
	 * @throws IOException
	 */
	public static void find(final File dirToSearch, final FileFilter filter, final Writer writer) throws IOException {
		try {
			for (File currentFile : dirToSearch.listFiles()) {
				if (filter.accept(currentFile)) {
					writer.write(currentFile.getAbsolutePath());
				}
				if (currentFile.isDirectory()) {
					find(currentFile, filter, writer);
				}
			}
		} catch (NullPointerException e) {
			System.err.println("unable to list files for " + dirToSearch);
		}
	}

	/**
	 * Notice: Not throughly tested!<p>
	 *
	 * Deletes a file. If the file is a directory,
	 * all files contained within the directory will also be deleted.
	 *
	 * If a file is unable to be overwritten, a message will be printed.
	 * A SecurityException may be thrown If a security manager exists.
	 *
	 */
	public static void delete(File file) throws IOException {
		if (file.isDirectory()) {
			for (File currentFile : file.listFiles()) {
				delete(currentFile);
			}
		}
		if (!file.delete()) {
			System.err.println("unable to delete " + file.getAbsolutePath());
		}
	}

	/**
	 * Notice: Not throughly tested!<p>
	 *
	 * Warning: If overWrite is set to true and merge is false and a file(directories included) exists at the
	 * destination, it and all of its contends will be deleted before the copy begins. If this copy fails for any reason,
	 * the file and it's contents that existed at the destination before the copy began will be lost.<p><p>
	 *
	 * Copies a file or directory from one location to another.<p>
	 *
	 *
	 */
	public static void copy(File source, File destination, boolean overwrite, boolean merge) throws IOException {
		System.out.println("copy " + source.getAbsolutePath() + " to " + destination.getAbsolutePath());

		if (!merge && destination.exists()) {
			if (overwrite) {
				delete(destination);
			} else {
				System.out.println("File already exists. Set overWrite to true if you would like to copy anyway.");
				return;
			}
		}

		if (source.isDirectory()) {
			if (overwrite && !merge) {
				Files.copy(source.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
			}

			for (File file : source.listFiles()) {
				copy(file, new File(destination.getAbsolutePath() + File.separator + file.getName()), overwrite, merge);
			}
		} else {
			Files.copy(source.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}
	}

	/**
	 * Copies source file to destination. If the destination file is a directory,
	 * the contents of the two directories will be merged.
	 * This will also applies to any sub-directories that may exist. Any files that are not a directory,
	 * will be overwritten.
	 *
	 *
	 * Notice: Not throughly tested!<p>
	 *
	 */
	public static void mergeOverwriteCopy(File source, File destination) throws IOException {
		System.out.println("mergeOverwriteCopy " + source.getAbsolutePath() + " to " + destination.getAbsolutePath());

		Files.copy(source.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);

		if (source.isDirectory()) {
			for (File file : source.listFiles()) {
				mergeOverwriteCopy(file, new File(destination.getAbsolutePath() + File.separator + file.getName()));
			}
		}
	}

	/**
	 *
	 * Notice: Not throughly tested!<p>
	 *
	 */
	public static void deleteDestAndCopy(File source, File destination) throws IOException {
		System.out.println("deleteDestAndcopy " + source.getAbsolutePath() + " to " + destination.getAbsolutePath());

		delete(destination);

		mergeOverwriteCopy(source, destination);

	}

	public static void writeDuplicateFilesTostdout(File topmostDir) throws IOException {
		try (BufferedWriter br = new BufferedWriter(new OutputStreamWriter(System.out))) {
			writeDuplicateFiles(topmostDir, br);
		}
	}

	/**
	 * Reads one char at a time
	 * @param topmostDir
	 * @param outputFile
	 * @throws IOException
	 */
	public static void writeDuplicateFilesToFile(File topmostDir, File outputFile) throws IOException {
		try (BufferedWriter br = new BufferedWriter(new FileWriter(outputFile))) {
			writeDuplicateFiles(topmostDir, br);
		}
	}

	/**
	 * Reads one char at a time
	 * Notice: Not fully tested.
	 * @throws IOException
	 *
	 */
	public static void writeDuplicateFiles(File topmostDir, Writer writer) throws IOException {
		//System.out.println("printDuplicateFiles(" + dir.getAbsolutePath() + ")");
		File[] files = topmostDir.listFiles();
		if (files == null) {
			writer.write("Unable to list files in " + topmostDir.getAbsolutePath() + System.lineSeparator());
			System.err.println("Unable to list files in " + topmostDir.getAbsolutePath());
			return;
		}
		for (int i = 0; i < files.length; i++) {

			if (files[i].isDirectory()) {
				writeDuplicateFiles(files[i], writer);
				continue;
			}

			for (int j = 0; j < files.length; j++) {

				if (i == j) {
					continue; //prevents file from being compared with itself
				}

				if (files[j].isDirectory()) {
					continue;
				}

				if (files[i].length() == files[j].length() && filesAreEqual(files[i], files[j])) {
					writer.write(files[i] + " == " + System.lineSeparator() + files[j] + System.lineSeparator()
							+ System.lineSeparator());
				} else {
					//System.out.println(files[i] + " != " + files[j]);
				}

			}

		}
	}

	/**
	 * Reads 1 char at a time
	 * @param file1
	 * @param file2
	 * @return
	 */
	public static boolean filesAreEqual(File file1, File file2) {

		try (FileReader fileReader1 = new FileReader(file1)) {

			try (FileReader fileReader2 = new FileReader(file2)) {

				while (true) {
					int read1 = fileReader1.read();
					int read2 = fileReader2.read();
					if (read1 != read2) {
						return false;
					} else if (read1 == -1) { //both are at end of file
						break;
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}

	/**
	 * Slightly modified version of a source from
	 * http://stackoverflow.com/questions/304268/getting-a-files-md5-checksum-in-java
	 * @param file
	 * @return
	 */
	public static String md5sum(File file) {
		try (InputStream fin = new FileInputStream(file)) {
			MessageDigest md5er = MessageDigest.getInstance("MD5");
			byte[] buffer = new byte[1024];
			int read;
			do {
				read = fin.read(buffer);
				if (read > 0) {
					md5er.update(buffer, 0, read);
				}
			} while (read != -1);
			byte[] digest = md5er.digest();
			if (digest == null) {
				return null;
			}
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < digest.length; i++) {
				sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (Exception e) {
			return null;
		}
	}

	public static String md5sum(String s) {
		try {
			MessageDigest md5er = MessageDigest.getInstance("MD5");
			md5er.update(s.getBytes());
			byte[] digest = md5er.digest();
			if (digest == null) {
				return null;
			}
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < digest.length; i++) {
				sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String sha1sum(String s) {
		try {
			MessageDigest md5er = MessageDigest.getInstance("SHA-1");
			md5er.update(s.getBytes());
			byte[] digest = md5er.digest();
			if (digest == null) {
				return null;
			}
			String strDigest = "";
			for (int i = 0; i < digest.length; i++) {
				strDigest += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1).toUpperCase();
			}
			return strDigest;
		} catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

}
