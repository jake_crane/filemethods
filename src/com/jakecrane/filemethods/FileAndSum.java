package com.jakecrane.filemethods;

import java.io.File;
import java.io.Serializable;

public class FileAndSum implements Serializable {

	private static final long serialVersionUID = -1674436018030184169L;
	private File file;
	private String md5sum;

	public FileAndSum(File file, String md5sum) {
		this.file = file;
		this.md5sum = md5sum;
	}

	public File getFile() {
		return file;
	}

	public String getMd5sum() {
		return md5sum;
	}

	@Override
	public boolean equals(Object obj) {
		return md5sum.equals(((FileAndSum)obj).getMd5sum());
	}

	@Override
	public String toString() {
		return md5sum + " " + file.getAbsolutePath();
	}
}