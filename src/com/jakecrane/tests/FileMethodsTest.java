package com.jakecrane.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.regex.Pattern;

import org.junit.Ignore;
import org.junit.Test;

import com.jakecrane.filemethods.FileMethods;


public class FileMethodsTest {

	@Ignore
	@Test
	public void testMd5sumofDirsFiles() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testMd5sumRepresentingDir2() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testWriteFilesAndSumsFileWriter() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testCollectChildrenInfo() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testWriteFilesAndSumsFileWriterArrayListOfFileAndSum() {
		fail("Not yet implemented");
	}

	@Test
	public void testLineInFileContainsFilePattern() throws IOException {
		File file = new File("testLineInFileContainsFilePattern.MyTempFile.tmp");
		file.deleteOnExit();
		String storedString = "123abc";
		Pattern matchingPattern = Pattern.compile("\\d{3}[a-zA-Z]{3}");
		Pattern nonMathcingPattern = Pattern.compile("\\d{4}");
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(storedString);
		}
		assertFalse(FileMethods.lineInFileContains(file, nonMathcingPattern));
		assertTrue(FileMethods.lineInFileContains(file, matchingPattern));
	}

	@Test
	public void testLineInFileContainsFileString() throws IOException {
		File file = new File("testLineInFileContainsFileString.MyTempFile.tmp");
		file.deleteOnExit();
		String storedString = "This is a test.";
		String nonMathcingString = "My unstored string";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(storedString);
		}
		assertFalse(FileMethods.lineInFileContains(file, nonMathcingString));
		assertTrue(FileMethods.lineInFileContains(file, storedString));
	}

	@Test
	public void testReplaceAllFileCharChar() throws IOException {
		File file = new File("testReplaceAllFileCharChar.tmp");
		file.deleteOnExit();
		String string = "abcdefgabcdefg";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(string);
		}
		assertTrue("replaceAll returned false.", FileMethods.replaceAll(file, 'a', 'b'));
		String stringFromFile = new String(Files.readAllBytes(file.toPath()));
		String expectedString = "bbcdefgbbcdefg";
		assertEquals("Replace did not work properly.", expectedString, stringFromFile);
	}
	
	@Test
	public void testReplaceAllFileCharCharRemoveChar() throws IOException {
		File file = new File("testReplaceAllFileCharCharRemoveChar.tmp");
		file.deleteOnExit();
		String string = "abcdefgabcdefg";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(string);
		}
		assertTrue("replaceAll returned false.", FileMethods.replaceAll(file, 'a', '\0'));
		String stringFromFile = new String(Files.readAllBytes(file.toPath()));
		String expectedString = "bcdefgbcdefg";
		assertEquals("Replace did not work properly.", expectedString, stringFromFile);
	}

	@Test
	public void testReplaceAllFileStringString() throws IOException {
		File file = new File("testReplaceAllFileStringString.tmp");
		file.deleteOnExit();
		String string = "abcdefgabcdefg" + System.lineSeparator();
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(string);
		}
		assertTrue("replaceAll returned false.", FileMethods.replaceAll(file, "a", "b"));
		String stringFromFile = new String(Files.readAllBytes(file.toPath()));
		String expectedString = "bbcdefgbbcdefg" + System.lineSeparator();
		assertEquals("Replace did not work properly.", expectedString, stringFromFile);
	}
	
	@Test
	public void testReplaceAllFileStringStringWithEmptyString() throws IOException {
		File file = new File("testReplaceAllFileStringStringWithEmptyString.tmp");
		file.deleteOnExit();
		String string = "abcdefgabcdefg" + System.lineSeparator();
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(string);
		}
		assertTrue("replaceAll returned false.", FileMethods.replaceAll(file, "a", ""));
		String stringFromFile = new String(Files.readAllBytes(file.toPath()));
		String expectedString = "bcdefgbcdefg" + System.lineSeparator();
		assertEquals("Replace did not work properly.", expectedString, stringFromFile);
	}

	@Ignore
	@Test
	public void testWriteFileTreeToFile() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testWriteFileTree() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testWriteMd5sumsOfAllFilesToFile() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testWriteMd5sumsOfAllFiles() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testMd5sumRepresentingDir() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDirContentsAreEqual() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testFindFileFileFilterListOfFile() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testFindFileFileFilter() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testFindFileFileFilterWriter() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() throws IOException {
		File testDir = new File("testDelete.TestDir");
		testDir.deleteOnExit();
		assertTrue("Test Directory created.", testDir.mkdir());
		File[] files = {new File(testDir, "testDelete.temp1"), new File(testDir, "testDelete.temp2"), new File(testDir, "testDelete.temp3")};
		for (File file : files) {
			assertTrue("Test file created.", file.createNewFile());
			file.deleteOnExit();
		}
		FileMethods.delete(testDir);
		assertTrue("Test directory has been deleted.", !testDir.exists());
	}

	@Ignore
	@Test
	public void testCopy() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testMergeOverwriteCopy() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDeleteDestAndCopy() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testWriteDuplicateFilesTostdout() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testWriteDuplicateFilesToFile() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testWriteDuplicateFiles() {
		fail("Not yet implemented");
	}

	@Test
	public void testFilesAreEqual() throws IOException {
		File file1 = new File("testFilesAreEqual.testFilesAreEqual1.tmp");
		file1.deleteOnExit();
		String matchingFileContents = "abc123";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file1))) {
			bw.write(matchingFileContents);
		}
		File file2 = new File("testFilesAreEqual.testFilesAreEqual2.tmp");
		file2.deleteOnExit();
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file2))) {
			bw.write(matchingFileContents);
		}
		File file3 = new File("testFilesAreEqual.testFilesAreEqual30.tmp");
		file3.deleteOnExit();
		String nonMatchingContents = "This does not match.";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file3))) {
			bw.write(nonMatchingContents);
		}
		assertTrue(FileMethods.filesAreEqual(file1, file2));
		assertFalse(FileMethods.filesAreEqual(file1, file3));
	}

	@Test
	public void testMd5sumFile() throws IOException {
		String plainText = "hello";
		String expectedMd5sum = "5d41402abc4b2a76b9719d911017c592";
		File file = new File("testMd5sumFile.testMd5sumFile.tmp");
		file.deleteOnExit();
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(plainText);
		}
		assertEquals(expectedMd5sum, FileMethods.md5sum(file));
	}

	@Test
	public void testMd5sumString() {
		String plainText = "hello";
		String expectedMd5sum = "5d41402abc4b2a76b9719d911017c592";
		assertEquals(expectedMd5sum, FileMethods.md5sum(plainText));
	}

	@Ignore
	@Test
	public void testSha1sum() {
		String plainText = "hello";
		String expectedSha1sum = "aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d";
		assertEquals(expectedSha1sum, FileMethods.sha1sum(plainText));
	}

}
